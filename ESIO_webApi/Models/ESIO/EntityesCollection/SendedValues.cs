﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ESIO_webApi.Models.ESIO.Entityes;

namespace ESIO_webApi.Models.ESIO.EntityesCollection
{
    public class SendedValues
    {
        private IQueryable<SendedValue> queryable;

        public SendedValues(IQueryable<SendedValue> queryable)
        {
            this.queryable = queryable;
            Configure();
        }

        private void Configure()
        {
            values = new List<SendedValue>();
            foreach(var item in this.queryable)
            {
                values.Add(item);
            }
        }

        public int ValuesCount { get; set; }
        public List<SendedValue> values { get; set; }
    }
}