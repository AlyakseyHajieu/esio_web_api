﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ESIO_webApi.Models.ESIO.Interface;

namespace ESIO_webApi.Models.ESIO.Entityes
{
    public class Subservice:IId
    {
        public int Id { get; set; }

        public virtual Service ParentService { get; set; }
        public int ParentId { get; set; }

        public string SubServiceDescription { get; set; }
        public DateTime SubServiceDatecreation { get; set; }
    }
}