﻿using ESIO_webApi.Models.ESIO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESIO_webApi.Models.ESIO.Entityes
{
    public class SendedValue:IId
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Long { get; set; }
    }
}
