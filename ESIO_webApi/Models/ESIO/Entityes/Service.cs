﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ESIO_webApi.Models.ESIO.Interface;

namespace ESIO_webApi.Models.ESIO.Entityes
{
    public class Service : IId
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreation { get; set; }
    }
}