﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using ESIO_webApi.Models.ESIO.Entityes;

namespace ESIO_webApi.Models.ESIO.MappingClasses
{
    public class ServiceMap:EntityTypeConfiguration<Service>
    {
        public ServiceMap()
        {
            ToTable("Services");
        }
    }
}