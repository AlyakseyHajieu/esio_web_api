﻿using ESIO_webApi.Models.ESIO.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESIO_webApi.Models.ESIO.MappingClasses
{
    public class SendedValuesMap:EntityTypeConfiguration<SendedValue>
    {
        public SendedValuesMap()
        {
            ToTable("SendedValuesTables");
        }
    }
}
