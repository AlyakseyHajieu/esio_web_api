﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESIO_webApi.Models.ESIO.Interface
{
    public interface IId
    {
        int Id { get; set; }
    }
}
