namespace ESIO_webApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        DateCreation = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubServices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        SubServiceDescription = c.String(),
                        SubServiceDatecreation = c.DateTime(nullable: false),
                        ParentService_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Services", t => t.ParentService_Id)
                .Index(t => t.ParentService_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubServices", "ParentService_Id", "dbo.Services");
            DropIndex("dbo.SubServices", new[] { "ParentService_Id" });
            DropTable("dbo.SubServices");
            DropTable("dbo.Services");
        }
    }
}
