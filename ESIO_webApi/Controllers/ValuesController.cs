﻿using ESIO_webApi.DAO.Repositoryes;
using ESIO_webApi.Models;
using ESIO_webApi.Models.ESIO.Entityes;
using ESIO_webApi.Models.ESIO.EntityesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using PushSharp;
using PushSharp.Google;

namespace ESIO_webApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public SendedValues Get()
        {
            var repo = new ValuesRepository(new ApplicationDbContext());
            var data = new SendedValues(repo.FinAll());
            return data;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post(SendedValue value)
        {
            var repo = new ValuesRepository(new ApplicationDbContext());
            repo.Create(value);
            NotifiClients();
        }

        private void NotifiClients()
        {
            
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
