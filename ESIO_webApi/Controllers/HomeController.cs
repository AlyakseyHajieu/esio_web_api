﻿using ESIO_webApi.DAO.Repositoryes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ESIO_webApi.Models;
using ESIO_webApi.Models.ESIO.Entityes;
using ESIO_webApi.Models.ESIO.EntityesCollection;

namespace ESIO_webApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var repo = new ValuesRepository(new ApplicationDbContext());
            var data = new SendedValues(repo.FinAll());
            return View(data.values);
        }

        public ActionResult Edit(int? Id)
        {
            var repo = new ValuesRepository(new ApplicationDbContext());
            var data = repo.FinAll().Where(curent => curent.Id == Id);
            return View(data);
        }

        public ActionResult AdministrativeDatas()
        {
            return null;
        }
    }
}
