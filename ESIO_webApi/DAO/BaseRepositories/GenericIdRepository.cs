﻿using ESIO_webApi.Models.ESIO.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESIO_webApi.DAO.BaseRepositories
{
    public abstract class GenericIdRepository<T> : IDisposable where T : class, IId
    {
        private readonly DbContext _dbContext;
        
        protected GenericIdRepository(DbContext unit)
        {
            _dbContext = unit;
        }

        public IQueryable<T>FinAll()
        {
            return _dbContext.Set<T>();
        }


        public void Create(T item)
        {
            _dbContext.Entry(item).State = EntityState.Added;
            _dbContext.SaveChangesAsync();    
        }


        public void Dispose()
        {
            _dbContext.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
