﻿using ESIO_webApi.DAO.BaseRepositories;
using ESIO_webApi.Models.ESIO.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESIO_webApi.DAO.Repositoryes
{
    public class ValuesRepository:GenericIdRepository<SendedValue>
    {
        public ValuesRepository(DbContext unit) : base(unit) { }
    }
}
