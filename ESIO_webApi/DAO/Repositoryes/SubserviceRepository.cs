﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ESIO_webApi.DAO.BaseRepositories;
using ESIO_webApi.Models.ESIO.Entityes;

namespace ESIO_webApi.DAO.Repositoryes
{
    public class SubserviceRepository : GenericIdRepository<Subservice>
    {
        public SubserviceRepository(DbContext unit) : base(unit)
        {
        }
    }
}